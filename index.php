<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Activity S2</title>
</head>
<body>
	<div>
		<h1>Divisibles of Fives</h1>
		<?php whileLoop(); ?>
	</div>

	<div>
	      <h2>Array Manipulation</h2>
	      <?php array_push($students, "John Smith") ?>
	      <p><?php var_dump($students) ?></p>
	      <p><?= count($students) ?></p>
	      <?php array_push($students, "Jane Smith") ?>
	      <p><?php var_dump($students) ?></p>
	      <p><?= count($students) ?></p>
	      <?php array_shift($students) ?>
	      <p><?php var_dump($students) ?></p>
	      <p><?= count($students) ?></p>
	   </div>

</body>
</html>